# Sa-Token-Plugin 第三方插件

Sa-Token第三方插件实现，基于Sa-Token-Core，提供一些与官方不同实现机制的的插件，未明确说明的情况下都是适用在Servlet环境下。版本号跟随Sa-Token官方版本号，当前版本号为 **`v1.25.0-SNAPSHOT`**，兼容最新Sa-Token的版本，所以不发布对应的新版本。

## 引入依赖

该插件的依赖未发布到maven中央仓库，引入时需要手动在根pom中添加一下`jitpack`镜像仓库，推荐将`jitpack`镜像放到最后一个

### 先添加repositories节点
```xml
<repositories>
    <!-- 放在其他镜像配置之后 -->
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

## 插件列表

### 1. [Redis存储插件](https://gitee.com/bootx/sa-token-plugin-redis)

- 介绍

  与官方实现思想不同，这个插件式聚合了官方多个`redis`插件包的功能，在原有功能基础上扩展了`FastJson` 序列化，后期将将支持`JRedis`和`Lucence`的配置切换。同时在配置方式上与官方插件有区别，通过继承`SaTokenRedisConfiguration` 后重写方法可以进行精细化的自定义配置。
> 注意，不可以与官方Redis存储插件一起引入，否则会有冲突

- 添加pom依赖

```xml
<dependency>
    <groupId>com.gitee.bootx</groupId>
    <artifactId>sa-token-plugin-redis</artifactId>
    <version>${sa-token.plugin.version}</version>
</dependency>
```

- yaml参数配置项

  ```yaml
  sa-token:
    plugins:
      redis:
        # 序列化方式, 支持JDK、JACKSON、FASTJSON三种
        serialization: fastjson
        # 是否开启独立redis存储
        alone: true
        # 独立redis存储配置，同redis配置项
        alone-redis:
          xxx: xxx
  ```

- 自定义配置

  ```java
  /**
  * 自定义配置
  * @author xxm
  * @date 2021/7/30
  */
  @Configuration
  @RequiredArgsConstructor
  public class MySaTokenDaoRedisConfiguration extends SaTokenRedisConfiguration {
  
      private final ObjectMapper objectMapper;
  
      /**
       * 自定义jdk序列化方式
       */
      @Override
      protected JdkSerializationRedisSerializer JdkRedisSerializer() {
          return super.JdkRedisSerializer();
      }
  
      /**
       * jackson序列化
       */
      @Override
      protected GenericJackson2JsonRedisSerializer jackson2JsonRedisSerializer() {
          return super.jackson2JsonRedisSerializer();
      }
  
      /**
       * 处理ObjectMapper对象,使用jackson序列化情况下, 可以重写这个方法进行自定义ObjectMapper，用于支持java8
       * LocalDateTime等一些类
       */
      @Override
      protected ObjectMapper objectMapper() {
          return super.objectMapper();
      }
  
      /**
       * 自定义FastJson序列化
       */
      @Override
      protected GenericFastJsonRedisSerializer fastJsonRedisSerializer() {
          return super.fastJsonRedisSerializer();
      }
  
      /**
       * 扩展redis存储时的序列化方式,默认提供jdk、Jackson和FastJson三种
       */
      @Override
      public RedisSerializer<Object> redisSerializer() {
          return super.redisSerializer();
      }
  
      /**
       * 自定义redis连接方式,当前提供lettuce方式的连接
       */
      @Override
      protected RedisConnectionFactory redisConnectionFactory() {
          return super.redisConnectionFactory();
      }
  
      /**
       * StringRedisTemplate
       */
      @Override
      protected StringRedisTemplate stringRedisTemplate() {
          return super.stringRedisTemplate();
      }
  
      /**
       * RedisTemplate 存储非字符串的数据是的redis客户端
       */
      @Override
      protected RedisTemplate<String, Object> objectRedisTemplate() {
          return super.objectRedisTemplate();
      }
  
      /**
       * 重写自定义的Sa-Token持久层接口
       */
      @Override
      public SaTokenDao saTokenDaoRedis() {
          return super.saTokenDaoRedis();
      }
  
  }
  ```

  

### 2. [忽略权限验证插件](https://gitee.com/bootx/sa-token-plugin-redis)

- 介绍

  有时候我们会有一些接口不需要登录就可以访问，但专门在配置文件中配置也比较麻烦，所以提供一个在接口上添加注解就可以忽略权限验证的插件。

- 添加pom依赖

  ```xml
  <dependency>
      <groupId>com.gitee.bootx</groupId>
      <artifactId>sa-token-plugin-ignore</artifactId>
      <version>${sa-token.plugin.version}</version>
  </dependency>
  ```

- 注册到拦截器中

  ```java
  @Configuration
  public class SaTokenConfigure implements WebMvcConfigurer {
   
        // 注册路由拦截器，自定义验证规则
        registry.addInterceptor(new SaRouteInterceptor((req, res, handler)->
                SaRouter.match(Collections.singletonList("/**"))
                        .notMatch(Collections.singletonList("/login"))
                        .check(new IgnoreSaRouteFunction(handler))
        )).addPathPatterns("/**");
  }
  ```

- 注解形式

  ```java
  // 添加 IgnoreAuth 注解后,将忽略本接口的权限认证,不适用与aop模式
  @IgnoreAuth
  @ApiOperation("测试")
  @GetMapping("/hello")
  public ResResult<String> hello(){
      return Res.ok("hello");
  }
  ```

  
